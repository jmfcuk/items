import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import logo from '../Img/logo.svg';
import './App.css';
import ItemList from '../Items/ItemList'

class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div className="App">
          <header className="App-header">
            <h1 className="App-title">Items</h1>
          </header>
          <ItemList />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
