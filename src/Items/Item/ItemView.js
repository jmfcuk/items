import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import ConfirmDlg from '../ConfirmDlg';

const ItemView = (props) => {
  return(
    <div>
      <div className="Item">
        <div>{props.name}</div>
        <div>{props.description}</div>
        <div>{props.itemUri}</div>
        <FlatButton label="Edit"  primary={true} onClick={props.startEdit} />
        <FlatButton label="Delete"  primary={true}
        onClick={() => {
              props.del();
        }} />           
        <FlatButton label="Close"  primary={true} onClick={props.close} />
      </div>
    </div>
  );
}

export default ItemView;
