import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import './Item.css';
import ConfirmDlg from '../ConfirmDlg';
import ItemView from './ItemView';
import ItemEdit from './ItemEdit';

class Item extends Component {

  constructor(props) {

    super(props);

    this.startEdit = this.startEdit.bind(this);
    this.endEdit = this.endEdit.bind(this);
    this.cancelEdit = this.cancelEdit.bind(this);
    this.delete = this.delete.bind(this);
    this.deleteConfirmed = this.deleteConfirmed.bind(this);
    this.closeItemView = this.closeItemView.bind(this);

    this.state = {
      editing: false,
      item: { id: this.props.id, 
              name: this.props.name,
              description: this.props.description,
              data: this.props.data,
              imageUri: this.props.imageUri,
              itemUri: this.props.itemUri
      },
      warnDelete: false
    }
  }

  closeItemView() {
    this.setState({editing: false, warnDelete: false});
    this.props.close();
  }

  startEdit() {
    this.setState({editing: true});
   }

  endEdit(it) {
    this.setState({editing: false, item: it});
    this.props.updateItem(it);
  }

  cancelEdit() {
    this.setState({ editing: false });
  }

   delete() {
     this.setState({ editing: false, warnDelete: true });
   }

  deleteConfirmed(confirm) {
    if(confirm) {
      let init = { method: 'DELETE',
      mode: 'cors',
      cache: 'default' };

      fetch(this.props.itemUri, init)
      .then(response => response.json())
      .then(data => {
      });

      this.props.deleteItem({id: this.props.id, uri: this.props.itemUri});
      this.setState({ editing: false, warnDelete: false})
    }
  }

  render() {

    let jsx = null;

    if(this.state.editing === true) {
      jsx = 
      (
        <ItemEdit id={this.state.item.id}
                  name={this.state.item.name}
                  description={this.state.item.description}
                  data={this.state.item.data}
                  itemUri={this.state.item.itemUri}
                  imageUri={this.state.item.imageUri}
                  endEdit={this.endEdit}
                  cancelEdit={this.cancelEdit} />
      );
    }
    else {
      jsx = 
      (
      <div>
        <ItemView name={this.state.item.name}
                  description={this.state.item.description} 
                  itemUri={this.state.item.itemUri}
                  imageUri={this.state.item.imageUri}
                  startEdit={this.startEdit}
                  del={this.delete}
                  close={this.closeItemView} />
        <ConfirmDlg name={this.state.item.name} 
                    open={this.state.warnDelete} 
                    result={this.deleteConfirmed}></ConfirmDlg> 
      </div>
      );
    }

    return jsx;
  }
}

export default Item;
