import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

class ItemEdit extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      item: { 
          id: this.props.id, 
          name: this.props.name,
          description: this.props.description,
          data: this.props.data,
          itemUri: this.props.itemUri,
          imageUrl: this.props.imageUrl
      },
    }

    this.nameChanged = this.nameChanged.bind(this);
    this.descriptionChanged = this.descriptionChanged.bind(this);
    this.endEdit = this.endEdit.bind(this);
    this.cancelEdit = this.cancelEdit.bind(this);
  }

  nameChanged(e) {
  
    let item = this.state.item;

    if(item) {

      item.name = e.target.value;
    }

    this.setState({ item: item });
  }

  descriptionChanged(e) {

    let item = this.state.item;

    if(item) {

      item.description = e.target.value;
    }

    this.setState({ item: item });
  }

  endEdit() {

    var hdrs = new Headers();
    hdrs.set('Content-Type', 'application/json');

    let init = { method: 'PUT',
                 mode: 'cors',
                 cache: 'default',
                 headers: hdrs,
                 body: JSON.stringify(this.state.item)
                };

    fetch(this.state.item.itemUri, init)
      .then(response => response.json())
      .then(data => {
      });
      
    this.props.endEdit(this.state.item);
  }

  cancelEdit() {
    this.props.cancelEdit();
  }

  render() {
    return(
        <div className="Item">
        <div><TextField key="txtName" 
                        floatingLabelText="Name" 
                        inputStyle={{ textAlign: 'center' }} 
                        defaultValue={this.state.item.name}
                        onChange={this.nameChanged} />
        </div>
        <div><TextField key="txtDescription" 
                        floatingLabelText="Description" 
                        inputStyle={{ textAlign: 'center' }} 
                        defaultValue={this.state.item.description}
                        onChange={this.descriptionChanged} />
          <div>{this.state.item.uri}</div>
          <div><FlatButton label="Save"  primary={true} onClick={this.endEdit} />
               <FlatButton label="Cancel"  primary={true} onClick={this.cancelEdit} /></div>
        </div>
      </div>  
    );
  }
}

export default ItemEdit;