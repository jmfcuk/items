import React, { Component } from 'react';
import './ItemList.css';
import Item from '../Items/Item/Item';
import {List, ListItem} from 'material-ui/List';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';

class ItemList extends Component {

  constructor(props) {

    super(props);

    this.state = { 
      items: [],
      selectedItem: null,
    }

    this.selectItem = this.selectItem.bind(this);
    this.updateItem = this.updateItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.close  = this.close.bind(this);
  }

  componentDidMount() {
    this.refreshList();
  }

  selectItem(item) {
    this.setState({selectedItem: item});
  }
  
  updateItem(it) {

    let filteredItems = this.state.items.filter(item => {
      return item.id !== it.id;
    });

    filteredItems.push(it);

    filteredItems.sort((l, r) => {

      const a = l.name.toUpperCase();
      const b = r.name.toUpperCase();

      if (a < b) {
        return -1;
      }
      if (a > b) {
        return 1;
      }
    
      return 0;
    });

    this.setState({ items: filteredItems});
  }

  close() {
    this.setState({ selectedItem: null });
  }

  deleteItem(it) {
  
    const filteredItems = this.state.items.filter(item => {
      return item.id !== it.id;
    });
    this.setState({ items: filteredItems, selectedItem: null });
  }

  refreshList() {

    fetch('http://127.0.0.1:5000/items')
      .then(response => response.json())
      .then(data => this.setState({ items: data.items }));
  }

  resetList() {

    fetch('http://127.0.0.1:5000/reset')
    .then(response => response.json())
    .then(data => this.setState({ items: data.items }));
  }

  render() {

    return (
      <div className="ItemList">
        <RaisedButton label="Refresh" primary={true} onClick={() => this.refreshList()} />
        <RaisedButton label="Reset" primary={true} onClick={() => this.resetList()} />
        <List>
          {this.renderList()}
        </List>
          <Dialog title="Item"
                  modal={true}
                  open={this.state.selectedItem !== null}>
                  {this.getSelectedItem()}
          </Dialog>
      </div>
    );
  }

  renderList() {
    if(this.state.items) {
    return this.state.items.map(item => (
      <ListItem key={item.id} primaryText={item.name} 
                onClick={this.selectItem.bind(this, item)} />
    ));
    } else {
      return (
        null
      );
    }
  }

  getSelectedItem() {

    let it = this.state.selectedItem;
    let item = (<Item/>);

    if(it !== null) {
      item = (
        <Item key={it.id} 
              id={it.id} 
              name={it.name} 
              description={it.description}
              itemUri={it.itemUri}
              imageUri={it.imageUri}
              data={it.data}
              deleteItem={this.deleteItem}
              updateItem={this.updateItem}
              close={this.close} />
      );
    }

    return item;
  }
}

export default ItemList;

