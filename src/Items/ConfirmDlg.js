import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

class ConfirmDlg extends Component {

    constructor(props) {
  
      super(props);
      this.state = {
        open: false
      };
    }

    componentWillReceiveProps(nextProps)
    {
        this.setState({open: nextProps.open});
    }
    
    yes = () => {
      this.props.result(true);
      this.setState({open: false});
    }

    no = () => {
      this.props.result(false);
      this.setState({open: false});
    }

    render() {
      const actions = [
        <FlatButton
          label="Yes"
          primary={true}
          onClick={this.yes}
        />,
        <FlatButton
          label="No"
          primary={true}
          onClick={this.no}
        />,
      ];
  
      return (
        <div>
          <Dialog className="Item"
            label="Delete?"
            actions={actions}
            modal={true}
            open={this.state.open}>
              Delete {this.props.name}?
          </Dialog>
        </div>
      );
    }
}

export default ConfirmDlg;